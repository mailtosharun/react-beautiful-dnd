import React from "react";
import styled from "styled-components";
import { Task } from "./task";
import { Droppable } from "react-beautiful-dnd";
import { ITask } from "../initialData";

const Container = styled.div`
  margin: 8px;
  border: 1px solid lightgray;
  border-radius: 2px;
  width: 200px;
  display:flex;
  flex-direction:column;
`;

const Title = styled.h3`
  padding: 8px;
  text-align:center;
`;

const TaskList = styled.div<any>`
  padding: 8px;
  background-color:${props=>props.isDraggingOver?'skyblue':'white'};
  min-height:200px;
`;

export const Column = (props: any) => {
  return (
    <Container>
      <Title>{props.column.title}</Title>
      <Droppable droppableId={props.column.id}>
        {(provided, snapshot) => {
          return (
            <TaskList
            ref={provided.innerRef}
            {...provided.droppableProps}
            isDraggingOver={snapshot.isDraggingOver}
            >
              {props.tasks.map((task:ITask,index:number) => {
                return <Task key={task.id} task={task} index={index}/>;
              })}
              {provided.placeholder}
            </TaskList>
          );
        }}
      </Droppable>
    </Container>
  );
};
