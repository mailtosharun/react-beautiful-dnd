import React from "react";
import styled from "styled-components";
import { Draggable } from "react-beautiful-dnd";

const Container = styled.div<any>`
  margin: 8px;
  border: 1px solid lightgray;
  margin-bottom: 8px;
  border-radius: 2px;
  font-size: 20px;
  padding: 8px;
  background-color:${props=>props.isDragging?'lightgreen':'white'};
  flex:1;
`;



export const Task = (props: any) => {
  return (
    <Draggable draggableId={props.task.id} index={props.index} >
      {(provided,snapshot) => {
        return <Container
        {...provided.draggableProps}
        {...provided.dragHandleProps} 
        ref={provided.innerRef}
        isDragging={snapshot.isDragging}
        >{props.task.content}
        </Container>;
      }}
    </Draggable>
  );
};
