const initialData: ITodo = {
  tasks: [
    { id: "task-1", content: "first task" },
    { id: "task-2", content: "second task" },
    { id: "task-3", content: "third task" },
    { id: "task-4", content: "forth task" },
  ],
  columns: [
    {
      id: "column-1",
      title: "To-Do",
      taskIds: ["task-1", "task-2", "task-3", "task-4"],
    },
    {
      id: "column-2",
      title: "In Progress",
      taskIds: [],
    },
    {
      id: "column-3",
      title: "Done",
      taskIds: [],
    },
  ],
  columnOrder: ["column-1", 
  "column-2","column-3"
],
};

export default initialData;

export interface ITodo {
  tasks: ITask[];
  columns: IColumn[];
  columnOrder: string[];
}

export interface ITask {
  id: string;
  content: string;
}

export interface IColumn {
  id: string;
  title: string;
  taskIds: string[];
}
