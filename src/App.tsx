import React from "react";
import { MainPage } from "./pages/mainPage";
import initialData from "./initialData";

const App = () => {
return(
    <MainPage initialData={initialData}/>
)
};

export default App;
