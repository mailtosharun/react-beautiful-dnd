import React, { useState } from "react";
import { DragDropContext, DropResult } from "react-beautiful-dnd";
import { IColumn, ITask } from "../initialData";
import { Column } from "../components/column";
import styled from "styled-components";

const Container = styled.div`
  display: flex;
`;

export const MainPage = (props: any) => {
  const [state, setState] = useState(props.initialData);

  const columns = state.columnOrder.map((columnId: any) => {
    const column: IColumn = state.columns.filter(
      (c: IColumn) => c.id === columnId
    )[0];
    const tasks = column.taskIds.map((taskId) => {
      return state.tasks.find((t: ITask) => t.id === taskId);
    });
    return <Column key={column.id} column={column} tasks={tasks} />;
  });

  const onDragEnd = (result: DropResult) => {
    const { destination, source, draggableId } = result;

    if (!destination) return;

    if (
      destination.droppableId === source.droppableId &&
      destination.index === source.index
    )
      return;

    const start = state.columns.filter(
      (c: IColumn) => c.id === source.droppableId
    )[0];
    const finish = state.columns.filter(
      (c: IColumn) => c.id === destination.droppableId
    )[0];

    if (start == finish) {
      const newTaskIds = Array.from(start.taskIds);
      newTaskIds.splice(source.index, 1);
      newTaskIds.splice(destination.index, 0, draggableId);

      const newColumn = {
        ...start,
        taskIds: newTaskIds,
      };

      const updatedColumns = replace(state.columns, newColumn);
      const newState = {
        ...state,
        columns: updatedColumns,
      };
      setState(newState);
      return;
    }

    const startTaskIds = Array.from(start.taskIds);
    startTaskIds.splice(source.index, 1);
    const newStart = {
      ...start,
      taskIds: startTaskIds,
    };
    const finishTaskIds = Array.from(finish.taskIds);
    finishTaskIds.splice(destination.index, 0, draggableId);
    const newFinish = {
      ...finish,
      taskIds: finishTaskIds,
    };

    let updatedColumns = replace(replace(state.columns, newStart), newFinish);
    const newState = {
      ...state,
      columns: updatedColumns,
    };
    setState(newState);
    return;
  };

  const replace = (arr1: any, arr2: any) => {
    return arr1.map((a: any) => [arr2].find((b: any) => b.id === a.id) || a);
  };

  const onDragStart = () => {
  
  };

  const onDragUpdate = () => {
  };

  return (
    <DragDropContext
      onDragEnd={onDragEnd}
      onDragStart={onDragStart}
      onDragUpdate={onDragUpdate}
    >
      <Container>{columns}</Container>
    </DragDropContext>
  );
};
